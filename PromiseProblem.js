/* 
Note
-Log each operation that you do.
-Try to minimize number of api calls if possible.
-Try to avoid waiting for log promise to finish.
-Sign in and getBooks Promise rejections must always give appropriate responseCode and reason.
    reason: string     example - Auth Failure  or Service failure
    responseCode: number -  example: 401 or 500
-Activity Logs should be stored in an array.


Q. Write a signIn function
Function signature: takes a single argument username and returns a promise.
Returned Promise's value is based on seconds (getSeconds) from current date.
only resolve the promise if seconds are greater than 30 

Q. Write a getBooks method that returns a promise
it picks a random integer between [0 and 1].
if integer picked is 1 ..it resolves the promise with list of Books
else rejects the promise saying service error.


Q. Execute sign In 
Once Signin is successful, make getBooks call.
Only make getBooks call if you are signed in.

Log the results for all possible operations.
Sign In - Success,
Sign In - Failure,
GetBooks - Success,
GetBooks - Failure.

Q. Write a logData method that always returns a resolved promise. It takes 1 second to finish its operation.
It Accepts the activity and saves the activity logs. 

Q. Write a function to display all activity logs.
Signature: accepts no Params.
Return:  name of the user that calls it and all its logs. 

Q. Perform sign in and getBooks calls for 2 Users (Mary, Emily)

Q. Finally display the logs for both of them.

*/

const Books = [
  {
    name: "Hostilities of War",
    _id: "book293492178",
  },
  {
    name: "A Beautiful Sunset",
    _id: "book293492178",
  },
  {
    name: "Lorem Ipsum",
    _id: "book293492178",
  },
  {
    name: "Rogue Asassin",
    _id: "book293492178",
  },
];

class Users {
  constructor(userName) {
    this.name = userName;
    this.activityLogs = [];
  }

  signIn() {
    return new Promise((res, rej) => {
      setTimeout(() => {
        const start = new Date().getSeconds();

        if (start > 30) {
          res(this.name);
        } else {
          // console.log("Sign In - Failed");
          rej({ reason: "Auth Failure", ResponseCode: 401 });
        }
      }, 500);
    });
  }

  getBooks() {
    return new Promise((res, rej) => {
      setTimeout(() => {
        const num = Math.round(Math.random());
        if (num == 1) {
          res(Books);
          // console.log("GetBooks - Success");
        } else {
          rej({ reason: "Service Failure", ResponseCode: 500 });
          // console.log("GetBooks - Failure");
        }
      }, 500);
    });
  }

  logData(activity) {
    return new Promise((res, rej) => {
      if (activity) {
        this.activityLogs.push(activity);
        res();
        // console.log("Logged " + activity);
      }
    });
  }
  displayAllLogs() {
    console.log(`Displaying Logs of ${this.name}:`);
    return this.activityLogs;
  }
}

const user2 = new Users("Emily");

user2
  .signIn()
  .then(
    (res) => {
      console.log(`${res} signed in`);
      user2.logData("Sign in successful");
      return user2.getBooks();
    },
    (rej) => {
      // console.log("Sign In -failed:" + rej.reason + rej.ResponseCode);
      user2.logData("Sign In -failed:" + rej.reason + " " + rej.ResponseCode);
      return Promise.reject(rej);
    }
  )
  .then(
    (res) => {
      user2.logData("GetBooks-Success");
      console.log(res);
    },
    (rej) => {
      user2.logData(
        "GetBooks - Failure " + rej.reason + " " + rej.ResponseCode
      );
    }
  ).finally(() => console.log(user2.displayAllLogs()));

  const user1 = new Users("Mary");

user1
  .signIn()
  .then(
    (res) => {
      console.log(`${res} signed in`);
      user1.logData("Sign in successful");
      return user1.getBooks();
    },
    (rej) => {
      // console.log("Sign In -failed:" + rej.reason + rej.ResponseCode);
      user1.logData("Sign In -failed:" + rej.reason + " " + rej.ResponseCode);
      return Promise.reject(rej);
    }
  )
  .then(
    (res) => {
      user1.logData("GetBooks-Success");
      console.log(res);
    },
    (rej) => {
      user1.logData(
        "GetBooks - Failure " + rej.reason + " " + rej.ResponseCode
      );
    }
  ).finally(() => console.log(user1.displayAllLogs()));

/*
const activityLogs = [];

function signIn(username) {
  return new Promise((res, rej) => {

    setTimeout(()=>{
    const start = new Date().getSeconds();

    if (start > 30) {
      res(username);
    } else {
      console.log("Sign In - Failed");
      rej({ reason: "Auth Failure", ResponseCode: 401 });
    }
 } ,500)
  })
}

function getBooks() {
  return new Promise((res, rej) => {

    setTimeout(()=>{
    const num = Math.round(Math.random());
    if (num == 1) {
      res(Books);
      console.log("GetBooks - Success");
    } else {
      rej({ reason: "Service Failure", ResponseCode: 500 });
      console.log("GetBooks - Failure");
    }
},500)
  });
}

function logData(activity) {
  return new Promise((res, rej) => {
    if (activity) {
    
      activityLogs.push(activity);
      res();
      console.log("Logged" + activity);
    }
  });
}

function displayAllLogs() {
  console.log("Displaying Logs:");
  return {
    User: userName,
    ActivityLogs: activityLogs.map((act) => console.log(act)),
  };
}*/

// signIn("Mary")
//   .then(
//     (username) => {
//       logData({ [username]: ["Sign In - Success"] });
//       return getBooks();
//     },
//     (err) => {
//       logData(err);
//     }
//   )
//   .catch((err) => console.log(err));
